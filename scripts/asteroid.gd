extends Flotsam

export(float, 0, 1) var DEPTH_RNG = 0.2
export(float, 0, 1) var NUG_RNG = 0.1
export(int, 1, 3)   var NUG_MULTIPLIER = 1
export(PackedScene) var nug_scn
onready var nugs = $nugs
var has_nugs := true

func _ready():
  randomize()
  axis_lock_linear_y = (randf() > DEPTH_RNG)
  if randf() > NUG_RNG:
    nugs.queue_free()
    has_nugs = false
  else:
    for nug in randi() % nugs.get_children().size():
      nugs.get_children()[nug].queue_free()


func _on_Asteroid_body_entered(body):
  if body is Flotsam:
    return
  if body is Bullet:
    if has_nugs:
      for _i in NUG_MULTIPLIER:
        for nug in nugs.get_children():
          var spot : Transform = nug.global_transform
          spot.origin.y = 0.0
          var nug_rb = nug_scn.instance()
          nug_rb.global_transform.origin = spot.origin
          get_tree().root.add_child(nug_rb)
          var impulse : Vector3 = nug.translation.normalized() * body.linear_velocity
          var torque  := Vector3(randf(),randf(),randf())
          nug_rb.apply_impulse(Vector3.ZERO, impulse)
          nug_rb.apply_torque_impulse(torque)
    queue_free()
