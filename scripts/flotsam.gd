extends RigidBody
class_name Flotsam

export(float) var KILL_TIMEOUT = 5.0
export(float) var INITIAL_KILL_TIMEOUT = 5.0

var timer := Timer.new()
onready var camera := get_tree().root.get_camera()


func _ready():
  add_child(timer)
  timer.connect("timeout", self, "_on_Timer_timeout")
  timer.start(INITIAL_KILL_TIMEOUT)


func _on_Timer_timeout():
  var in_view := true
  var pos := global_transform.origin
  for plane in camera.get_frustum():
    var to_me : Vector3 = (pos - plane.project(pos)).normalized()
    if plane.normal.dot(to_me) > 0.0:
      in_view = false
      break
  if in_view:
    timer.start(KILL_TIMEOUT)
  else:
    queue_free()
