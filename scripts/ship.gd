extends Player

#---------------------------------------------#
#  INDEX                                      #
#---------------------------------------------#
# VARIABLE_DECLARATIONS
#   VARS_PARAMS
#   VARS_SHOOTING
#   VARS_MOUSE
# METHOD_OVERRIDES
# METHOD_DEFINITIONS



#---------------------------------------------#
#  VARIABLE_DECLARATIONS                      #
#---------------------------------------------#

# VARS_PARAMS
export(float) var SPEED = 15
export(float) var DEADZONE = 0.2
export(float) var TURNFACTOR = 0.2
export(float) var FIRE_RATE = 0.2


# VARS_SHOOTING
export(Resource) var bullet_resource
onready var bullet_scn : PackedScene = load(bullet_resource.resource_path)
var shoot_timer := Timer.new()
var gun_no := 0
onready var guns = [
  $Spaceship/GunLeft1,
  $Spaceship/GunRight1,
  $Spaceship/GunLeft2,
  $Spaceship/GunRight2,
]


# VARS_MOUSE
onready var camera := get_tree().root.get_camera()
onready var reticle := $Reticle
var mouse : InputEventMouse
var mouse_dirty := false



#---------------------------------------------#
#  METHOD_OVERRIDES                           #
#---------------------------------------------#

func _ready():
  add_child(shoot_timer)
  shoot_timer.wait_time = FIRE_RATE
  shoot_timer.connect("timeout", self, "_on_ShootTimer_timeout")


func _process(delta):
  process_movement(delta)
  process_turning()
  process_fire()


func _unhandled_input(event):
  if event is InputEventMouseMotion:
    mouse = event
    mouse_dirty = true
    reticle.visible = true



#---------------------------------------------#
#  METHOD_DEFINITIONS                         #
#---------------------------------------------#

func process_movement(delta):
  var m_h := Input.get_action_strength("east") - Input.get_action_strength("west")
  var m_v := Input.get_action_strength("south") - Input.get_action_strength("north")
  var move := Vector3(m_h, 0.0, m_v)
  if move.length_squared() <= DEADZONE*DEADZONE:
    move = Vector3.ZERO
  elif move.length_squared() > 1.0:
    move = move.normalized()
  move_and_slide(move * SPEED)


var look := Vector3.FORWARD
func process_turning():
  var l_h := Input.get_action_strength("right") - Input.get_action_strength("left")
  var l_v := Input.get_action_strength("down") - Input.get_action_strength("up")
  var tlook := -Vector3(l_h, 0.0, l_v)

  if mouse_dirty:
    var reticlepos = camera.project_position(mouse.position, camera.global_transform.origin.y)
    reticle.global_transform.origin = reticlepos
    look = (global_transform.origin - reticlepos).normalized()
    mouse_dirty = false
  elif tlook.length_squared() > DEADZONE*DEADZONE:
    look = tlook.normalized()
    reticle.visible = false

  if is_equal_approx(transform.basis.z.dot(look), -1.0):
    look = transform.basis.x * sign(randf() - 0.5)

  var zbasis := look
  if not transform.basis.z.cross(look).is_equal_approx(Vector3.ZERO):
    zbasis = transform.basis.z.slerp(look, TURNFACTOR)
  var xbasis := transform.basis.y.cross(zbasis)

  transform.basis = Basis(xbasis, transform.basis.y, zbasis).orthonormalized()


func process_fire():
  if Input.is_action_just_pressed("fire"):
    fire()
    shoot_timer.start(FIRE_RATE)


func fire():
  var b := bullet_scn.instance()
  gun_no = (gun_no + 1) % guns.size()
  b.transform = guns[gun_no].global_transform
  get_tree().root.add_child(b)


func _on_ShootTimer_timeout():
  if Input.is_action_pressed("fire"):
    fire()
    shoot_timer.start(FIRE_RATE)
