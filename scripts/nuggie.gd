extends Flotsam

func _ready():
  connect("body_entered", self, "_on_Nuggie_body_entered")

func _on_Nuggie_body_entered(body):
  if body is Player:
    queue_free()
