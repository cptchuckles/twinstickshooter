extends Camera

export(NodePath) var env_node
onready var env := (get_node(env_node) as WorldEnvironment).environment

export(float) var SNAPFACTOR = 0.2
export(float) var SCROLLSPEED = 100.0
export(NodePath) var target_node
onready var target : Spatial = get_node(target_node) as Spatial

func _process(delta):
  var newpos : Vector3 = lerp(global_transform.origin, target.global_transform.origin, SNAPFACTOR)
  var diff := newpos - global_transform.origin 
  global_transform.origin = newpos
  if diff.length_squared() > 0.0: 
    env.background_sky_orientation = env.background_sky_orientation.rotated(
        transform.basis.z.cross(diff).normalized(),
        deg2rad(diff.length_squared() * SCROLLSPEED * delta)
        )
