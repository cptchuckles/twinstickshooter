extends RigidBody
class_name Bullet

export(float) var FORCE = 25
export(float) var LIFETIME = 2.0


var timer := Timer.new()


func _ready():
  add_child(timer)
  timer.connect("timeout", self, "_on_Timer_timeout")
  timer.start(LIFETIME)
  apply_impulse(transform.basis.z, -transform.basis.z * FORCE * mass)


func _on_Timer_timeout():
  queue_free()


func _on_PlasmaRound_body_entered(_body):
  queue_free()
